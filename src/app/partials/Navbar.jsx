"use client";
import React, { useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import { gsap } from "gsap";
import Head from "next/head";
import Image from "next/image";

import Link from "next/link";
import { Squash as Hamburger } from "hamburger-react";

const Header = () => {
  const [isOpen, setOpen] = useState(false);
  const [isScrollingUp, setIsScrollingUp] = useState(false);

  const routes = [
    { path: "/", title: "Work" },
    { path: "/about", title: "About" },
    { path: "/services", title: "Services" },
    { path: "/ideas", title: "Ideas" },
    { path: "/careers", title: "Careers" },
    { path: "/contact", title: "Contact" },
  ];

  const pathname = usePathname();

  useEffect(() => {
    let lastScrollTop = 0;
    const navbar = document.getElementById("navbar");

    const handleScroll = () => {
      let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      setIsScrollingUp(scrollTop < lastScrollTop);
      lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <nav className="fixed top-0 left-0 z-[9999] w-full" id="navbar">
      <div className="w-full z-[9999] bg-primary">
        <header className={`header mx-auto lg:max-w-screen-2xl lg:px-4 py-4 justify-between flex items-center shadow-smooth-lg shadow-primary`}>
          <Image
            height={1000}
            width={1000}
            src="/images/logo_suitmedia.png"
            className="h-[20%] w-[20%] md:h-[10%] md:w-[12%]"
            alt="logo suitmedia"
          />

          <div
            className={`flex lg:hidden items-center border border-primary ${
              isOpen ? "bg-primary" : "bg-transparent"
            } h-10 w-[50px] rounded-lg justify-center`}
          >
            <Hamburger
              easing="ease-in"
              toggled={isOpen}
              toggle={setOpen}
              color={isOpen ? "#ff" : "#fff"}
              size={20}
              hideOutline={false}
            />
          </div>

          <div className="hidden lg:flex items-center font-reddit-sans text-[16px]">
            <ul className="flex gap-4">
              {routes.map((route) => (
                <li
                  key={route.path}
                  className={`pb-2 border-b-2 ${
                    pathname === route.path
                      ? "text-white border-white transition duration-300"
                      : "text-white border-transparent"
                  }`}
                >
                  <Link href={route.path}>{route.title}</Link>
                </li>
              ))}
            </ul>
          </div>
        </header>
      </div>

      <div
        className={`mobile w-full justify-end ${
          isOpen ? "flex transition delay-300" : "hidden"
        }`}
      >
        <div className="h-full bg-white flex justify-end px-4 rounded-bl-xl shadow-primary shadow-md">
          <div className="font-reddit-sans text-[16px] py-8 items-end">
            <ul className="flex flex-col gap-4">
              {routes.map((route) => (
                <li
                  key={route.path}
                  className={`border rounded-md px-3 text-center ${
                    pathname === route.path
                      ? "text-primary border-primary transition duration-300"
                      : "text-primary border-white"
                  }`}
                >
                  <Link href={route.path}>{route.title}</Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;



