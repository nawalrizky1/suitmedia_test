"use client";
import Image from "next/image";
import { useState, useEffect } from "react";
import axios from "axios";
import { format } from "date-fns";

export default function Catalog() {
  const [card, setCard] = useState([]);
  const [sortedCard, setSortedCard] = useState("published_at");
  const [appendedCard, setAppendedCard] = useState("medium_image");
  const [pageSizes, setPageSizes] = useState(8);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  const fetchCard = async () => {
    try {
      const response = await axios.get(
        `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${page}&page[size]=${pageSizes}&append[]=${appendedCard}&sort=${sortedCard}`
      );
      setCard(response.data.data);
      setTotalPages(response.data.meta.last_page); 
    
    } catch (error) {
      console.error("Error fetching ideas:", error);
    }
  };

  useEffect(() => {
    fetchCard();
  }, [pageSizes, sortedCard, page]);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    return format(date, "d MMMM yyyy");
  };

  const goToPage = (pageNumber) => {
    setPage(pageNumber);
  };

  const goToPreviousPage = () => {
    if (page > 1) {
      setPage((prevPage) => prevPage - 1);
    }
  };

  const goToNextPage = () => {
    if (page < totalPages) {
      setPage((prevPage) => prevPage + 1);
    }
  };

 const renderPageNumbers = () => {
   const pages = [];
   const maxPageButtons = 4;
   let startPage = Math.max(1, page - Math.floor(maxPageButtons / 2));
   let endPage = startPage + maxPageButtons - 1;

   if (endPage > totalPages) {
     endPage = totalPages;
     startPage = Math.max(1, endPage - maxPageButtons + 1);
   }

   for (let i = startPage; i <= endPage; i++) {
     pages.push(
       <button
         key={i}
         onClick={() => goToPage(i)}
         className={`px-4 py-2 rounded-md  ${
           i === page ? "font-bold text-white bg-primary" : ""
         }`}
       >
         {i}
       </button>
     );
   }
   return pages;
 };

  return (
    <div className="flex flex-col w-full relative overflow-x-clip -mt-40">
      <div className="bg-white h-[50vh] flex w-[140%] absolute top-[-3%] lg:top-[-10%] left-[-10%] -rotate-[6deg]" />
      <div className="bg-white min-h-screen flex flex-col justify-center items-center text-black p-8 lg:p-16 z-10">
        <div className="flex flex-row w-full justify-between">
          <div className="w-full hidden lg:flex"></div>
          <div className="w-full flex gap-4 justify-end py-4">
            <div className="flex flex-row gap-4 items-center">
              <p className="text-sm lg:text-base">Show per page: </p>
              <select
                className="p-2 border border-gray-300 rounded-lg text-sm lg:text-base"
                value={pageSizes}
                onChange={(e) => setPageSizes(parseInt(e.target.value, 10))}
              >
                <option value={8}>8</option>
                <option value={16}>16</option>
                <option value={24}>24</option>
              </select>
            </div>
            <div className="flex flex-row gap-4 items-center">
              <p className="text-sm lg:text-base">Sort by:</p>
              <select
                className="p-2 border border-gray-300 rounded-lg text-sm lg:text-base"
                value={sortedCard}
                onChange={(e) => setSortedCard(e.target.value)}
              >
                <option value="-published_at">Newest</option>
                <option value="published_at">Oldest</option>
              </select>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-2 lg:grid-cols-4 w-full gap-6">
          {card.map((card) => (
            <div
              key={card.id}
              className="flex flex-col w-full relative overflow-clip rounded-lg lg:rounded-xl shadow-xl hover:scale-105 transition-transform duration-500 ease-in-out"
            >
              <Image
                src="https://placehold.co/600x400/png"
                width={1000}
                height={1000}
                className="h-auto w-full object-cover "
                alt="Placeholder Image"
                loading="lazy"
              />
              <div className="flex flex-col flex-wrap p-4">
                <p className="text-xs lg:text-sm text-slate-400 font-semibold">
                  {formatDate(card.published_at)}
                </p>
                <p className="line-clamp-3 text-sm lg:text-xl hover:text-primary  font-bold">
                  {card.title}
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className="flex justify-center mt-20">
          <button
            onClick={() => goToPage(1)}
            disabled={page === 1}
            className="mr-4 px-4 py-2 font-bold text-slate-500"
          >
            &lt;&lt;
          </button>
          <button
            onClick={goToPreviousPage}
            disabled={page === 1}
            className="mr-4 px-4 py-2 font-bold text-slate-500"
          >
            &lt;
          </button>
          {renderPageNumbers()}
          <button
            onClick={goToNextPage}
            disabled={page === totalPages}
            className="ml-4 px-4 py-2 font-bold text-slate-500"
          >
            &gt;
          </button>
          <button
            onClick={() => goToPage(totalPages)}
            disabled={page === totalPages}
            className="ml-4 px-4 py-2 font-bold text-slate-500 "
          >
            &gt;&gt;
          </button>
        </div>
        <div className="flex justify-center mt-4">
          <p className="text-gray-600">
            Page {page} of {totalPages}
          </p>
        </div>
      </div>
    </div>
  );
}
