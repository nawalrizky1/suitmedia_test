import Hero from "@/app/component/module/ideas/Hero";
import Catalog from "@/app/component/module/ideas/Catalog";
export default function Ideas() {
  return (
    <div className="relative">
      <Hero />
      <Catalog />
    </div>
  );
}
