export default function Page() {
  return (
    <main className="flex min-h-screen bg-primary">
      <div className="flex flex-col items-center justify-center w-full">
        <h1 className="text-6xl">This is Services!</h1>
      </div>
    </main>
  );
}
