import Image from "next/image";

export default function Home() {
  return (
    <main className="flex min-h-screen bg-primary">
      <div className="flex flex-col items-center justify-center w-full">
        <h1 className="text-6xl">This is Work!</h1>
      </div>
    </main>
  );
}
